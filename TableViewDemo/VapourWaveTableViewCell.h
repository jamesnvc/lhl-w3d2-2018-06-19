//
//  VapourWaveTableViewCell.h
//  TableViewDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VapourWaveTableViewCell : UITableViewCell

@property (nonatomic,strong) NSString* info;

// in practice, instead of NSString*, this would probably take whatever your "data object" is
- (void)configureCellWithData:(NSString*)info indexPath:(NSIndexPath*)indexPath;

@end
