//
//  VapourWaveTableViewCell.m
//  TableViewDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "VapourWaveTableViewCell.h"

@interface VapourWaveTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *fancyLabel;
@property (strong, nonatomic) IBOutlet UIButton *uselessButton;
@property (strong,nonatomic) UIView *weirdThingView;

@end

@implementation VapourWaveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithData:(NSString *)info indexPath:(NSIndexPath *)indexPath
{
    self.info = info; // make the cell know what data it's representing
    self.fancyLabel.text = [info uppercaseString];
    [self.uselessButton
     setTitle:[NSString stringWithFormat:@"Row %ld", indexPath.row + 1]
     forState:UIControlStateNormal];
    if (indexPath.row % 2 == 0) {
        self.weirdThingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 44)];
        self.weirdThingView.backgroundColor = UIColor.cyanColor;
        [self.contentView addSubview:self.weirdThingView];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.weirdThingView removeFromSuperview];
    self.weirdThingView = nil;
}

@end
