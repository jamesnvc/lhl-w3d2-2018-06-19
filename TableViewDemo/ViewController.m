
//
//  ViewController.m
//  TableViewDemo
//
//  Created by James Cash on 19-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "VapourWaveTableViewCell.h"

@interface ViewController () <UITableViewDataSource>

// using the <> to indicate what type of objects the data array contains
// just so XCode doesn't complain when we do things like data[0].count
// note though that this does *not* actually check this -- we could still put any sort of object in the array & it wouldn't complain
@property (nonatomic,strong) NSArray<NSArray<NSString*>*>* data;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.data = @[@[@"foo", @"bar"],
                  @[@"baz", @"quux", @"aoeu"],
                  @[@"foobar", @"hello!"]];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // check which segue this is
//    dest = segue.destinationViewController
    // because we're already using a custom subclass for our cell, which is also the sender of this segue, we can just make our cell "know" what piece of data it's representing & just get it directly
    NSLog(@"Send this info to destination: %@", ((VapourWaveTableViewCell*)sender).info);
    // OR
    /*
    UITableView* tableView; // pretend this is an outlet to the tableview
    NSIndexPath *index = [tableView indexPathForCell:sender];
    NSString *info = self.data[index.section][index.row];
     */

}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // optional, if we don't implement it, will assume to be one
    return self.data.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data[section].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // we do this instead of saying [[UITableViewCell alloc] init] because we don't want to have to create a whole new view for every single row (what if we have 100,000 rows?)
    // instead, we let the tableview reuse cells, so when one scrolls off screen, we can reuse it for whatever is scrolling on screen from the other edge
    VapourWaveTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"GreenCell"];

    // get the data item corresponding to this row
    NSString *item = self.data[indexPath.section][indexPath.row];

    // this will be where we customize the cell
    [cell configureCellWithData:item indexPath:indexPath];

    // one approach to get "outlets" to our cell if we don't want to make a custom subclass (which is what you probably should do, but this is a useful trick) is to "tag" the views & use withWithTag: to find the subview
    /*
    UILabel *fancyLabel = [cell viewWithTag:1];
    fancyLabel.text = item;
    UIButton *cellButton = [cell viewWithTag:2];
    [cellButton setTitle:[NSString stringWithFormat:@"Row %ld", indexPath.row] forState:UIControlStateNormal];
     */

//    cell.textLabel.text = item;
//    cell.textLabel.text = [NSString stringWithFormat:@"Hello S %ld R %ld", indexPath.section, indexPath.row];

    // Example of what *not* to do
    // when we do this, we see the same cells come up repeatedly and we just see rows 0-17 repeatedly (instead of 0-99)
    // (the solution is to not "initialize" properties of the cell here, in the way that here we're trying to check if we've already given the label a value, or more commonly in practice, creating a new view & assigning it to the cell)
    /*
    if (![cell.textLabel.text hasPrefix:@"ROW"]) {
        cell.textLabel.text = [NSString stringWithFormat:@"ROW %ld", indexPath.row];
    }
     */

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"Section %ld", section];
}

@end
